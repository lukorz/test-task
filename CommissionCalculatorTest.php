<?php

namespace TestTask;

use PHPUnit\Framework\TestCase;

/**
 * commissionCalculatorTest
 *
 * @author Sakul
 */
class CommissionCalculatorTest extends TestCase
{
    /**
     * @var CommissionCalculator
     */
    private $commissionCalculator;

    /**
     * Test printCommissions() File open error
     */
    public function testPrintCommissions1()
    {
        $this->expectOutputString('Error opening file. Make sure file exists');
        $this->commissionCalculator->printCommissions('asd');
    }

    /**
     * Test printCommissions() JSON format error
     */
    public function testPrintCommissions2()
    {
        $file = 'test.txt';
        $this->expectOutputString('Check file format');
        file_put_contents($file, 'asd');
        $this->commissionCalculator->printCommissions('test.txt');
        unlink($file);
    }

    /**
     * Test printCommissions() Working case
     */
    public function testPrintCommissions3()
    {
        $file = 'test.txt';
        $this->expectOutputString('1
0.44
1.64
2.28
44.17
');
        $testData = '{"bin":"45717360","amount":"100.00","currency":"EUR"}
            {"bin":"516793","amount":"50.00","currency":"USD"}
            {"bin":"45417360","amount":"10000.00","currency":"JPY"}
            {"bin":"41417360","amount":"130.00","currency":"USD"}
            {"bin":"4745030","amount":"2000.00","currency":"GBP"}';
        file_put_contents($file, $testData);
        $this->commissionCalculator->printCommissions('test.txt');
        unlink($file);
    }

    /**
     * Test isEu()
     */
    public function testIsEu()
    {
        $reflection = new \ReflectionObject($this->commissionCalculator);
        $method = $reflection->getMethod('isEu');
        $method->setAccessible(true);
        $expectedEuCountry = $method->invoke($this->commissionCalculator, 'PT');
        $this->assertTrue($expectedEuCountry);
        $expectedNotEuCountry = $method->invoke($this->commissionCalculator, 'BR');
        $this->assertFalse($expectedNotEuCountry);
    }

    protected function setUp(): void
    {
        $this->commissionCalculator = new CommissionCalculator();
    }
}
