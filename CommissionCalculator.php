<?php

namespace TestTask;

/**
 * commissionCalucator
 */
class CommissionCalculator
{
    const RATES_PROVIDER_URL = 'https://api.exchangeratesapi.io/latest';
    const BIN_LOOKUP_BASE_URL = 'https://lookup.binlist.net/';

    public function printCommissions(string $file)
    {
        try {
            $handle = fopen($file, "r");
        } catch (\Exception $e) {
            echo 'Error opening file. Make sure file exists';

            return;
        }

        while ($line = fgets($handle)) {
            $values = json_decode($line, true);

            if (!$values) {
                echo 'Check file format';

                return;
            }

            $binResults = file_get_contents(self::BIN_LOOKUP_BASE_URL.$values['bin']);

            if (!$binResults) {
                echo 'Unable to retrieve bin results for bin value '.$values['bin'].', skipping';

                continue;
            }

            if ('EUR' != $values['currency']) {
                $rates = file_get_contents(self::RATES_PROVIDER_URL);

                if (!$rates) {
                    echo 'Unable to retrieve rates';

                    return;
                }

                $decodedRates = json_decode($rates, true);
                $rate = $decodedRates['rates'][$values['currency']];
                $values['amount'] = 0 == $rate ? $values['amount'] : $values['amount'] / $rate;
            }

            $isEu = $this->isEu(json_decode($binResults, true)['country']['alpha2']);
            $amountCommissioned = $values['amount'] * ($isEu ? 0.01 : 0.02);
            $decimalCount = strlen(substr(strrchr((string) $amountCommissioned, "."), 1));

            echo $decimalCount > 2 ? ceil($amountCommissioned*100)/100 : $amountCommissioned;
            echo "\n";
        }
    }

    private function isEu($c)
    {
        $euCountryCodes = ['AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'EL',
            'ES', 'FI', 'FR', 'GR', 'HR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV',
            'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK'
        ];

        return in_array($c, $euCountryCodes);
    }
}
