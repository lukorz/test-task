<?php

namespace TestTask;

require_once __DIR__.'/CommissionCalculator.php';

(new CommissionCalculator())->printCommissions($argv[1]);
